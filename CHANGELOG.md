# Changelog

## [v0.1.0]

* Initial release of finam-vtk

[unpublished]: https://git.ufz.de/FINAM/finam-vtk/-/compare/v0.1.0...main
[v0.1.0]: https://git.ufz.de/FINAM/finam-vtk/-/commits/v0.1.0
