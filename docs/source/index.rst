:html_theme.sidebar_secondary.remove: true

============
FINAM VTK
============

VTK reader and writer components for the `FINAM <https://finam.pages.ufz.de/>`_ model coupling framework.

Quickstart
----------

Installation:

.. code-block:: bash

    pip install finam-vtk

For available components, see the :doc:`api/index`.

Usage
-----

See the `example scripts <https://git.ufz.de/FINAM/finam-vtk/-/tree/main/examples>`_
in the GitLab repository for fully functional usage examples.

Readers
^^^^^^^

The package provides two types of VTK reader components:

* :class:`.VTKStaticReader` for reading starting conditions during initialization
* :class:`.PVDReader` for reading time series of rasters

Both components can read multiple variables from a single dataset.

:class:`.VTKStaticReader`
"""""""""""""""""""""""""

Reads once during initialization of the coupling setup.
A time slice must be provided if the variable is actually temporal.

.. code-block:: Python

    from finam_vtk import VTKStaticReader
    from datetime import datetime

    path = "tests/data/head.vtu"
    reader = VTKStaticReader(path, reference_date=datetime(2024, 5, 1), time_unit="seconds")

:class:`.PVDReader`
"""""""""""""""""""

Reads once on each time step, where time steps are defined in the PVD file.
Timepoints are calculated by the given reference date and a time unit.

.. code-block:: Python

    from finam_vtk import PVDReader

    path = "tests/data/head.pvd"
    reader = PVDReader(path, time)

When multiple variables/layers are read, they must all use the same time dimension (i.e. they must have common time steps).

Outputs
"""""""

Component outputs can be accessed by the keys used for `outputs`, e.g. for linking:

.. code-block:: Python

    reader.outputs["lai"] >> viewer.inputs["Grid"]

Writers
^^^^^^^

The package provides two types of VTK writer components:

* :class:`.VTKTimedWriter` for writing in predefined, fixed time intervals
* :class:`.VTKPushWriter` for writing whenever new data becomes available

Both components can write multiple variables to a single dataset.

:class:`.VTKTimedWriter`
""""""""""""""""""""""""

Writes time slices regularly, irrespective of input time steps.

.. code-block:: Python

    from datetime import timedelta
    from finam_vtk import VTKTimedWriter

    path = "tests/data/out.nc"
    reader = VTKTimedWriter(path, ["lai"], step=timedelta(days=1))

:class:`.VTKPushWriter`
"""""""""""""""""""""""

Writes time slices as soon as new data becomes available to the inputs.
Note that all input data sources must have the same time step!

.. code-block:: Python

    from finam_vtk import VTKPushWriter

    path = "tests/data/out.nc"
    reader = VTKPushWriter(path, ["lai"])

API References
--------------

Information about the API of FINAM-VTK.

.. toctree::
    :hidden:
    :maxdepth: 1

    self

.. toctree::
    :maxdepth: 1

    api


About
-----

Further information about licensing, the developers team and the changelog of FINAM-VTK.

.. toctree::
  :maxdepth: 2

  about/index


License
^^^^^^^

LGPLv3, Copyright © 2021-2024, the FINAM developers from Helmholtz-Zentrum für Umweltforschung GmbH - UFZ. All rights reserved.
