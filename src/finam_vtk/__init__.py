"""
FINAM components VTK file I/O.

.. toctree::
   :hidden:

   self

Readers
=======

.. autosummary::
   :toctree: api
   :caption: Readers

    PVDReader
    VTKStaticReader

Writers
=======

.. autosummary::
   :toctree: api
   :caption: Writers

    VTKPushWriter
    VTKTimedWriter

Tools
=====

.. autosummary::
   :toctree: api
   :caption: Tools

    DataArray
    read_vtk_grid
"""

from . import tools
from .reader import PVDReader, VTKStaticReader
from .tools import DataArray, read_vtk_grid
from .writer import VTKPushWriter, VTKTimedWriter

try:
    from ._version import __version__
except ModuleNotFoundError:  # pragma: no cover
    # package is not installed
    __version__ = "0.0.0.dev0"

__all__ = [
    "tools",
    "VTKStaticReader",
    "PVDReader",
    "VTKPushWriter",
    "VTKTimedWriter",
    "DataArray",
    "read_vtk_grid",
]
