# finam-vtk

FINAM components for reading and writing spatial and temporal data from and to [VTK](https://vtk.org/) files.

See the finam-vtk [documentation](https://finam.pages.ufz.de/finam-vtk).

## License

LGPLv3, Copyright © 2021-2024, the FINAM developers from Helmholtz-Zentrum für Umweltforschung GmbH - UFZ. All rights reserved.
