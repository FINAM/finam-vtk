"""
VTK writer components.
"""

import finam as fm


class VTKTimedWriter(fm.TimeComponent):
    """
    VTK writer component that writes in predefined time intervals.

    Parameters
    ----------
    path : str
        Path to the VTK file to read.
    inputs : list of str or Variable.
        List of inputs. Input is either defined by name or a :class:`Variable` instance.
    step : datetime.timedelta
        Time step
    time_var : str or None, optional
        Name of the time coordinate. To create a static output file, set this to None.
        By default: "time"
    global_attrs : dict, optional
        global attributes for the VTK file inputed by the user.
    """


class VTKPushWriter(fm.Component):
    """
    VTK writer component that writes on push to its inputs.

    Parameters
    ----------
    path : str
        Path to the VTK file to read.
    inputs : list of str or Variable.
        List of inputs. Input is either defined by name or a :class:`Variable` instance.
    time_var : str
        Name of the time coordinate.
    time_unit : str, optional
        time unit given as a string: days, hours, minutes or seconds.
    global_attrs : dict, optional
            global attributes for the VTK file inputed by the user.
    """
